import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import actions from '../store/authActions';

class Home extends Component {
    logoutClick = event => {
        this.props.logout();
    }

    render() {
        return (
            <div>
                {
                    this.props.auth.isLogined
                        ?
                        <h2>Hello, {this.props.auth.email} <br />  <Link to={'/'} onClick={this.logoutClick}>logout</Link> </h2>
                        :
                        <h2>Please <Link to={'/Login'}>login</Link> or <Link to={'/Registration'}>register</Link></h2>
                }
            </div>
        )
    }
};

let mapProps = (state) => {
    return {
        auth: state.Auth
    }
}

let mapDispatch = (dispatch) => {
    return {
        logout: () => dispatch(actions.logout())
    }
}

export default connect(mapProps, mapDispatch)(Home)
