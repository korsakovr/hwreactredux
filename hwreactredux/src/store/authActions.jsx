const login = (token, email) => {
    return {
        type: 'LOG_IN',
        token: token,
        email: email
    }
}

const logout = () => {
    return {
        type: 'LOG_OUT'
    }
}


export default {
    login,
    logout
}

