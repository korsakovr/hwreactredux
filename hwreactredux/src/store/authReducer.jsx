const initialState = {
    Auth: { token: '', email: '', isLogined: false }
}

const Auth = (state = initialState, action) => {
    switch (action.type) {
        case 'LOG_IN':
            return {
                ...state,
                Auth: { token: action.token, email: action.email, isLogined: true }
            }
        case 'LOG_OUT':
            return {
                ...state,
                Auth: { token: '', email: '', isLogined: false }
            }

        default:
            return state
    }
}

export default Auth;
