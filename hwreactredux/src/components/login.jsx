import React, { Component } from 'react'
import { connect } from 'react-redux';
import actions from '../store/authActions';
import axios from 'axios';
import { Navigate } from 'react-router';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: (props.email) ? props.email : '',
            password: (props.password) ? props.password : '',
            navigate:false
        };
        this.submitForm = () => ev => {
            ev.preventDefault();
            axios.post('https://reqres.in/api/login', {
                email: this.state.email, password: this.state.password
            })
                .then((resp) => {
                    this.props.login(resp.data.token, this.state.email);
                    this.setState({ navigate: true });
                })
                .catch(function (error) {
                    alert(error.response.data.error);
                    return;
                });
        };

    }

    handleEmailChanged = event => {
        this.setState({ email: event.target.value });
    }

    handlePasswordChanged = event => {
        this.setState({ password: event.target.value });
    }

    render() {
        if (this.state.navigate) {
            return <Navigate to="/" replace={true} />
        }
        return (
            <form onSubmit={this.submitForm()}>
                <h3>Sign In</h3>
                <p>email: <b>eve.holt@reqres.in</b></p>
                <p>password: <b>cityslicka</b></p>
                <div className="mb-3">
                    <label>Email address</label>
                    <input
                        type="email"
                        className="form-control"
                        placeholder="Enter email"
                        value={this.state.email}
                        onChange={this.handleEmailChanged}
                    />
                </div>
                <div className="mb-3">
                    <label>Password</label>
                    <input
                        type="password"
                        className="form-control"
                        placeholder="Enter password"
                        value={this.state.password}
                        onChange={this.handlePasswordChanged}
                    />
                </div>
                <div className="d-grid">
                    <button type="submit" className="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        )
    }
}

let mapDispatch = (dispatch) => {
    return {
        login: (token, email) => dispatch(actions.login(token, email))
    }
}


export default connect(null, mapDispatch)(Login)